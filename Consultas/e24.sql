select e.mgr, max(e.sal), min(e.sal)
from emp x join emp e
on x.empno=e.mgr
where x.deptno!= 10
group by e.mgr
having min(e.sal)>1000;

select empno, ename, sal, deptno
from emp a
where (deptno, sal) in
	(select deptno, max(sal)
	from emp
	group by deptno);

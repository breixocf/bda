select e.empno, ename, p.prono, pname, d.deptno, dname, hours
from emp e join emppro ep 
on e.empno=ep.empno
join pro p
on ep.prono=p.prono
join dept d
on p.deptno=d.deptno;

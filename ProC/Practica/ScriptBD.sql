/* ***************** */
/* BORRADO DE TABLAS */
/* ***************** */

DROP TABLE equipo CASCADE CONSTRAINTS;
DROP TABLE jugador CASCADE CONSTRAINTS;
DROP TABLE entrenador CASCADE CONSTRAINTS;
DROP TABLE arbitro CASCADE CONSTRAINTS;
DROP TABLE partido CASCADE CONSTRAINTS;
DROP TABLE tipo_incidencia CASCADE CONSTRAINTS;
DROP TABLE incidencia CASCADE CONSTRAINTS;
DROP TABLE persona CASCADE CONSTRAINTS;
DROP SEQUENCE sq_partido;
DROP SEQUENCE sq_incidencia;
DROP SEQUENCE sq_equipo;

/* ****************** */
/* CREACION DE TABLAS */
/* ****************** */
CREATE TABLE persona(
	dni VARCHAR(30),
	nombre VARCHAR(30) NOT NULL,
	apellidos VARCHAR(30) NOT NULL,
	peso INT,
	altura INT,
	CONSTRAINT pk_persona PRIMARY KEY (dni)
);

CREATE TABLE tipo_incidencia(
	id_tipo INT,
	nombre VARCHAR(30),
	CONSTRAINT pk_tipo_incidencia PRIMARY KEY (id_tipo)
);

CREATE TABLE entrenador(
	dni VARCHAR(9),
	nivel VARCHAR(30),
	equipo INT,
	CONSTRAINT pk_entrenador PRIMARY KEY (dni),
	CONSTRAINT fk_entrenador_EN FOREIGN KEY (dni) REFERENCES persona(dni) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE equipo(
	id_equipo INT,
	nombre VARCHAR(30),
	estadio VARCHAR(30),
	entrenador VARCHAR(30),
	CONSTRAINT pk_eq PRIMARY KEY (id_equipo),
	CONSTRAINT fk_entrenador_EQ FOREIGN KEY (entrenador) REFERENCES entrenador(dni) ON UPDATE CASCADE ON DELETE SET NULL
);
CREATE SEQUENCE sq_equipo INCREMENT by 1 START WITH 1;

CREATE TABLE jugador(
	dni VARCHAR(30),
	posicion VARCHAR(30) NOT NULL,
	equipo INT,
	CONSTRAINT pk_jugador PRIMARY KEY (dni),
	CONSTRAINT fk_jugador_J FOREIGN KEY (dni) REFERENCES persona(dni) ON UPDATE CASCADE ON DELETE CASCADE,	
	CONSTRAINT fk_equipo_J FOREIGN KEY (equipo) REFERENCES equipo(id_equipo) ON UPDATE CASCADE ON DELETE CASCADE	
);

CREATE TABLE arbitro(
	dni VARCHAR(30),
	colegio VARCHAR(30),
	CONSTRAINT pk_arbitro PRIMARY KEY (dni),
	CONSTRAINT fk_arbitro_A FOREIGN KEY (dni) REFERENCES persona(dni) ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE partido(
	id_partido INT,
	local INT,
	visitante INT,
	arbitro VARCHAR(30),
	CONSTRAINT pk_partido PRIMARY KEY (id_partido),
	CONSTRAINT fk_local_PA FOREIGN KEY (local) REFERENCES equipo(id_equipo) ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT fk_visitante_PA FOREIGN KEY (visitante) REFERENCES equipo(id_equipo) ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT fk_arbitro_PA FOREIGN KEY (arbitro) REFERENCES arbitro(dni) ON UPDATE CASCADE ON DELETE SET NULL
);
CREATE SEQUENCE sq_partido INCREMENT by 1 START WITH 1;


CREATE TABLE incidencia(
	id_incidencia INT,
	minuto INT,
	partido INT,
	jugador VARCHAR(30),
	tipo INT,
	CONSTRAINT pk_incidencia PRIMARY KEY (id_incidencia),
	CONSTRAINT fk_partido_I FOREIGN KEY (partido) REFERENCES partido(id_partido) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT fk_jugador_I FOREIGN KEY (jugador) REFERENCES jugador(dni) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT fk_tipo_incidencia_I FOREIGN KEY (tipo) REFERENCES tipo_incidencia(id_tipo) ON UPDATE CASCADE ON DELETE SET NULL
);
CREATE SEQUENCE sq_incidencia INCREMENT by 1 START WITH 1;

/* Jugadores */
INSERT INTO persona (dni, nombre, apellidos, peso, altura) VALUES ('77418922W', 'Breixo', 'Camiña Fernández', 62, 177);
INSERT INTO persona (dni, nombre, apellidos, peso, altura) VALUES ('12345678a', 'Rubén', 'Ríos Brea', 64, 177);
INSERT INTO persona (dni, nombre, apellidos, peso, altura) VALUES ('23456789b', 'Bea', 'Iglesias Rodríguez', 71, 177);
INSERT INTO persona (dni, nombre, apellidos, peso, altura) VALUES ('34567891c', 'Brais', 'Couce Castro', 70, 180);
INSERT INTO persona (dni, nombre, apellidos, peso, altura) VALUES ('45678912d', 'Xoán', 'Mallón Moure', 105, 185);
INSERT INTO persona (dni, nombre, apellidos, peso, altura) VALUES ('56789123e', 'Álvaro', 'Feal Fajardo', 90, 181);
INSERT INTO persona (dni, nombre, apellidos, peso, altura) VALUES ('67891234f', 'Xabi', 'Costas Fiel', 74, 177);
/* Árbitros */
INSERT INTO persona (dni, nombre, apellidos, peso, altura) VALUES ('78945612g', 'Yaiza', 'Blanco Bermúdez', 55, 162);
INSERT INTO persona (dni, nombre, apellidos, peso, altura) VALUES ('89456123h', 'Marcos', 'Horro Varela', 62, 175);
/* Entrenadores */
INSERT INTO persona (dni, nombre, apellidos, peso, altura) VALUES ('90123456i', 'Javi', 'Gonzalez Velasco', 102, 180);
INSERT INTO persona (dni, nombre, apellidos, peso, altura) VALUES ('01234567j', 'Fran', 'Rua Barreiro', 88, 177);

INSERT INTO tipo_incidencia (id_tipo, nombre) VALUES (1,'GOL');
INSERT INTO tipo_incidencia (id_tipo, nombre) VALUES (2,'TARJETA AMARILLA');
INSERT INTO tipo_incidencia (id_tipo, nombre) VALUES (3,'TARJETA ROJA');

INSERT INTO entrenador (dni, nivel, equipo) VALUES ('90123456i', 'BAJO', 1);
INSERT INTO entrenador (dni, nivel, equipo) VALUES ('01234567j', 'MEDIO', 2);

INSERT INTO equipo (id_equipo, nombre, estadio, entrenador) VALUES (sq_equipo.NEXTVAL, 'BARCELONA', 'CAMP NOU', '90123456i');
INSERT INTO equipo (id_equipo, nombre, estadio, entrenador) VALUES (sq_equipo.NEXTVAL, 'REAL MADRID', 'SANTIAGO BERNABEU', '01234567j');

INSERT INTO jugador (dni, posicion, equipo) VALUES ('77418922W', 'DELANTERO', 1);
INSERT INTO jugador (dni, posicion, equipo) VALUES ('12345678a', 'DEFENSA', 1);
INSERT INTO jugador (dni, posicion, equipo) VALUES ('23456789b', 'MEDIO', 2);
INSERT INTO jugador (dni, posicion, equipo) VALUES ('34567891c', 'DELANTERO', 2);
INSERT INTO jugador (dni, posicion, equipo) VALUES ('45678912d', 'DEFENSA', 2);
INSERT INTO jugador (dni, posicion, equipo) VALUES ('56789123e', 'PORTERO', 1);
INSERT INTO jugador (dni, posicion, equipo) VALUES ('67891234f', 'PORTERO', 2);

INSERT INTO arbitro (dni, colegio) VALUES ('78945612g', 'GALLEGO');
INSERT INTO arbitro (dni, colegio) VALUES ('89456123h', 'MADRILEÑO');

INSERT INTO partido (id_partido, local, visitante, arbitro) VALUES(sq_partido.NEXTVAL, 1, 2, '78945612g');
INSERT INTO partido (id_partido, local, visitante, arbitro) VALUES(sq_partido.NEXTVAL, 2, 1, '89456123h');

INSERT INTO incidencia (id_incidencia, minuto, partido, jugador, tipo) VALUES (sq_incidencia.NEXTVAL, 15, 1, '77418922W', 1);
INSERT INTO incidencia (id_incidencia, minuto, partido, jugador, tipo) VALUES (sq_incidencia.NEXTVAL, 25, 1, '77418922W', 2);
INSERT INTO incidencia (id_incidencia, minuto, partido, jugador, tipo) VALUES (sq_incidencia.NEXTVAL, 45, 2, '56789123e', 2);
INSERT INTO incidencia (id_incidencia, minuto, partido, jugador, tipo) VALUES (sq_incidencia.NEXTVAL, 65, 2, '77418922W', 1);
INSERT INTO incidencia (id_incidencia, minuto, partido, jugador, tipo) VALUES (sq_incidencia.NEXTVAL, 71, 2, '34567891c', 2);
INSERT INTO incidencia (id_incidencia, minuto, partido, jugador, tipo) VALUES (sq_incidencia.NEXTVAL, 87, 2, '23456789b', 3);

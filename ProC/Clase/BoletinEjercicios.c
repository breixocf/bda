#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sqlca.h>

#define MAXLEN 40
#define MAXOPTS 10

EXEC SQL BEGIN DECLARE SECTION;
	char usuario[MAXLEN], clave[MAXLEN], nome[MAXLEN];
	int cod, prezo, cantidad, opt;
EXEC SQL END DECLARE SECTION;

void get_string(char *s, int maxlen){
	fgets(s,maxlen,stdin);
	int last = strlen(s) -1;
	if ( (s[last]=='\r') || (s[last]=='\n') )
		s[last] = '\0';
}

int get_int(){
	char s[MAXLEN];
	get_string(s,MAXLEN);
	return atoi(s);
}

int menu(){
	int opcion = -1;
	printf("\nMenu da aplicacion\n");
	printf("==================\n");
	printf("1. Obter datos (texto)\n");
	printf("2. Obter datos (numericos)\n");
	printf("3. Crear taboa ARTIGO\n");
	printf("4. Eliminar taboa ARTIGO\n");
	printf("5. Insertar ARTIGO\n");
	printf("6. Borrar ARTIGO por ID\n");
	printf("7. Borrar ARTIGO por NOME\n");
	printf("0. Sair\n");

	while ((opcion < 0) || (opcion > MAXOPTS)){
		printf("OP> ");
		opcion=get_int();
	}
	return opcion;
}

void ObterTexto(){
	char texto[MAXLEN];
	printf("Introduce un texto: ");
	get_string(texto, MAXLEN);
	printf("Texto introducido: |%s|\n", texto);
}

void ObterNumero(){
	int enteiro;
	printf("Introduce un número enteiro: ");
	enteiro = get_int();
	printf("Enteiro introducido: |%d|\n", enteiro);
}

void mostrarError(){
	EXEC SQL WHENEVER SQLERROR CONTINUE;
	switch(sqlca.sqlcode){
		case -942:
			printf("\n *** La tabla no existe. No se puede eliminar. *** \n");
			break;
		case -955:
			printf("\n *** La tabla ya está creada. No se puede crear. *** \n");
			break;
		case -1017:
			printf("\n *** Credenciales no válidas. *** \n");
			break;
		default:
			printf("\n *** Erro: Código %d, Mensaxe: %.*s. *** \n", sqlca.sqlcode, sqlca.sqlerrm.sqlerrml, sqlca.sqlerrm.sqlerrmc);
			break;
	}
}

void CrearTaboaArtigo(){
	EXEC SQL WHENEVER SQLERROR DO mostrarError();
	EXEC SQL CREATE TABLE artigo(
		codart NUMBER(4),
		nomart VARCHAR(40) NOT NULL,
		prezoart NUMBER(3),
		CONSTRAINT pk_artigo PRIMARY KEY(codart)
	);
	if(!sqlca.sqlcode)
		printf("Tabla creada con éxito\n");
	EXEC SQL COMMIT;
}

void EliminarTaboaArtigo(){
	EXEC SQL WHENEVER SQLERROR DO mostrarError();
	EXEC SQL DROP TABLE artigo;
	if(!sqlca.sqlcode)
		printf("Tabla eliminada con éxito\n");
	EXEC SQL COMMIT;
}

void InsertarArtigo(){
	EXEC SQL WHENEVER SQLERROR DO mostrarError();

	printf("Introduce un codigo de artigo: ");
	cod = get_int();

	printf("Introduce un nome de artigo: ");
	get_string(nome, MAXLEN);

	printf("Introduce un prezo de artigo: ");
	prezo = get_int();

	EXEC SQL INSERT INTO artigo(codart, nomart, prezoart) VALUES (:cod, :nome, :prezo);
	if(!sqlca.sqlcode){
		printf("\n\nFila insertada con éxito\n");
		EXEC SQL COMMIT;
	}
}

void BorrarArtigo(){
	EXEC SQL WHENEVER SQLERROR DO mostrarError();
	EXEC SQL WHENEVER NOTFOUND DO printf("\nArtigo non atopado.\n");
	printf("Introduce un codigo de artigo: ");
	cod = get_int();
	EXEC SQL DELETE FROM artigo WHERE codart=:cod;
	if(!sqlca.sqlcode){
		printf("\n\nFila eliminada con éxito\n");
		EXEC SQL COMMIT;
	}
}

void BorrarArtigoPatron(){
	EXEC SQL WHENEVER SQLERROR DO mostrarError();
	EXEC SQL WHENEVER NOTFOUND DO printf("\nArtigo non atopado.\n");
	printf("Introduce un nome: ");
	strcpy(nome, "%");
	get_string(nome, MAXLEN);
	strcpy(nome, "%");
	EXEC SQL DELETE FROM artigo WHERE nomart LIKE :nome;
	if(!sqlca.sqlcode){
		printf("\n\n%d filas eliminadas correctamente.\n", sqlca.sqlerrd[2]);
		EXEC SQL COMMIT;
	}
}

void CantidadArtigos(){
	EXEC SQL WHENEVER SQLERROR DO mostrarError();
	EXEC SQL WHENEVER NOTFOUND DO printf("\nArtigo non atopado.\n");
	EXEC SQL SELECT COUNT(*) INTO :cantidad FROM artigo;
	if(!sqlca.sqlcode){
		printf("\n\nA táboa artigo contén %d artigos.\n", cantidad);
		EXEC SQL COMMIT;
	}
}

void DetallesArtigo(){
	EXEC SQL WHENEVER SQLERROR DO mostrarError();
	EXEC SQL WHENEVER NOTFOUND DO printf("\nArtigo non atopado.\n");
	printf("Introduce un codigo de artigo: ");
	cod = get_int();
	EXEC SQL SELECT nomart, prezoart INTO :nome, :prezo FROM artigo WHERE codart=:cod;
	if(!sqlca.sqlcode){
		printf("\n\nArtigo %d - %s - %d euros.\n", cod, nome, prezo);
		EXEC SQL COMMIT;
	}
}

void ListadoArtigos(){
	EXEC SQL WHENEVER SQLERROR DO mostrarError();
	EXEC SQL WHENEVER NOTFOUND DO printf("\nArtigo non atopado.\n");
	EXEC SQL DECLARE cursor_artigos CURSOR FOR
	SELECT codart, nomart, prezoart FROM artigo;
	EXEC SQL OPEN cursor_artigos;
	EXEC SQL WHENEVER NOT FOUND DO BREAK;
	while(1) {
		EXEC SQL FETCH cursor_artigos  INTO :cod, :nome, :prezo;
		printf("\n\nArtigo %d - %s - %d euros.\n", cod, nome, prezo);
	}
	EXEC SQL WHENEVER NOT FOUND CONTINUE;
	printf("Artigos atopados: %d.\n", sqlca.sqlerrd[2]);
	EXEC SQL CLOSE cursor_artigos;
}

void ListadoArtigosPrezoMaior(){
	EXEC SQL WHENEVER SQLERROR DO mostrarError();
	EXEC SQL WHENEVER NOTFOUND DO printf("\nArtigo non atopado.\n");
	printf("Introduce un prezo de artigo: ");
	prezo = get_int();
	EXEC SQL DECLARE cursor_artigos CURSOR FOR
	SELECT codart, nomart, prezoart FROM artigo WHERE prezoart > :prezo;
	EXEC SQL OPEN cursor_artigos;
	EXEC SQL WHENEVER NOT FOUND DO BREAK;
	while(1) {
		EXEC SQL FETCH cursor_artigos  INTO :cod, :nome, :prezo;
		printf("\n\nArtigo %d - %s - %d euros.\n", cod, nome, prezo);
	}
	EXEC SQL WHENEVER NOT FOUND CONTINUE;
	printf("Artigos atopados: %d.\n", sqlca.sqlerrd[2]);
	EXEC SQL CLOSE cursor_artigos;
}

void SubirPrezoArtigo(){
	EXEC SQL WHENEVER SQLERROR DO mostrarError();
	EXEC SQL WHENEVER NOTFOUND DO printf("\nArtigo non atopado.\n");
	printf("Introduce un codigo de artigo: ");
	cod = get_int();
	printf("Introduce un porcentaxe de subida de prezo: ");
	prezo = get_int();
	EXEC SQL UPDATE artigo SET prezoart = (prezoart / 100) * :prezo WHERE codart = :cod;

	printf("¿Queres actualizar o prezo? (1:SI | 2:NO): ");
	opt = get_int();

	if(opt==1){
		printf("\nFila actualizada correctamente.\n");
		EXEC SQL COMMIT;
	}else{
		printf("\nFila no actualizada\n");
	}
}

int main(){
	int op;
	EXEC SQL WHENEVER SQLERROR DO mostrarError();
	do {
		printf("Introduce usuario: ");
		get_string(usuario, MAXLEN);
		printf("Introduce contraseña: ");
		get_password(clave, MAXLEN);
		EXEC SQL CONNECT :usuario IDENTIFIED BY :clave;
	} while(sqlca.sqlcode!=0);

	while (op=menu()){
		switch(op){
			case 1: ObterTexto(); break;
			case 2: ObterNumero(); break;
			case 3: CrearTaboaArtigo(); break;
			case 4: EliminarTaboaArtigo(); break;
			case 5: InsertarArtigo(); break;
			case 6: BorrarArtigo(); break;
			case 7: BorrarArtigoPatron(); break;
			case 0: EXEC SQL COMMIT RELEASE; exit(0);
		}
	}

	return 0;
}

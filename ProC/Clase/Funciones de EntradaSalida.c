/* 
 *  2014. Miguel Rodríguez Penabad (miguel.penabad@udc.es).
 * 
 * Funcións útiles para simplificar a entrada de datos por teclado ós 
 * programas en Pro*C 
 * 
 * - void get_string(char* s, int maxlen): Acepta por teclado un texto e 
 * 		recupera en s un string de como moito 'maxlen - 1' caracteres.
 * 
 * - void get_password(char* s, int maxlen): Acepta por teclado un texto e
 * 		recupera en s un string de como moito 'maxlen - 1' caracteres, 
 * 		pero non se ven na pantalla os caracteres tecleados.
 * 
 * - int get_int(): Acepta por teclado un texto, convírteo a enteiro, e
 * 		devolve o valor numérico. ver "man atoi" para saber o que ocorre
 * 		se introducimos un número inválido.
 * 
 * - float get_float(): Acepta por teclado un texto, convírteo a un número
 * 		en punto floante (un float) e devolve ese valor. Ver "man atof" 
 * 		para saber o que ocorre se non introducimos un número válido.
 * 
 * - void menu(): Escribe un menú na pantalla e solicita unha opción 
 * 		numérica entre 0 e MAXOPTS, e devolve esa opción. Se se introduce 
 * 		un número maior	que MAXOPTS ou menor que 0 volve a pedilo, pero 
 * 		se se introduce un texto que non é un número válido é posible que 
 * 		o convirta a unha opción válida.
 * 
 */


#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/* Máxima lonxitude para os números */
#define MAXLEN 20
/* Número de opcións do menú (excluida a opción de saír) */
#define MAXOPTS 2


void get_string(char *s, int maxlen){
	fgets(s,maxlen,stdin);
	int last = strlen(s) -1;
	if ( (s[last]=='\r') || (s[last]=='\n') )
		s[last] = '\0';
}

void get_password(char *s, int maxlen){
	system("stty -echo");
	get_string(s,maxlen);
	system("stty echo");
}

int get_int(){
	char s[MAXLEN];
	get_string(s,MAXLEN);
	return atoi(s);
}

float get_float(){
	char s[MAXLEN];
	get_string(s,MAXLEN);
	return atof(s);
}

int menu()
{
  int opcion = -1;

  printf("Menú da aplicación\n");
  printf("==================\n\n");
  printf("1. Obter datos (texto)\n");
  printf("2. Obter datos (numéricos)\n");
  printf("0. Saír\n");

  while ( (opcion < 0) || (opcion > MAXOPTS)){
      printf("OP> ");
      opcion=get_int();
  }
  return opcion;
}


/* Para probar */

/* Funcionalidade de exemplo número 1 */
void funcionalidade1()
{
	printf("Executando opción para a funcionalidade 1: obter datos (texto).\n");
	
	char texto[40], clave[30];
	
	printf("Introduce un texto: ");
	get_string(texto, 40);
	printf("Texto introducido: |%s|\n", texto);

	printf("Introduce a clave (non se verá): ");
	get_password(clave, 40);
	printf("Clave introducida: |%s|\n", clave);
}

/* Funcionalidade de exemplo número 2 */
void funcionalidade2()
{
	printf("Executando opción para a funcionalidade 2: obter datos (numéricos)\n");
	
	int enteiro;
	float real;
		
	printf("Introduce un número enteiro: ");
	enteiro = get_int();
	printf("Enteiro introducido: |%d|\n", enteiro);

	printf("Introduce un número real: ");
	real = get_float();
	printf("Real introducido: |%f|\n", real);
}

int main(){
	
	int op;

	while ( (op=menu()) != 0){
       switch(op){
            case 1: funcionalidade1(); break;
            case 2: funcionalidade2(); break;
       }
	}	
	
  return 0;
}
